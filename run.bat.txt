@echo off

call setEnv.bat

set JAVA_OPTS=%JAVA_OPTS%
set LOG_DIR=%BASE_DIR%\logs
set VERSION=1.0

set EXEC_JAR=%EXEC%-%VERSION%.jar

set CMD="%BASE_DIR%\lib\%EXEC_JAR%"

cd %BASE_DIR%

"%JAVA_HOME%\bin\java" %JAVA_OPTS% -jar %CMD% %ARGS%